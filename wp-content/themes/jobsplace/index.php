<?php get_header(); ?>
<div class="faixa-super">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-3 col-xs-12">
				<div class="logo_home"> work	<span class="sub_logo">ON DEMAND</span></div>
			</div>
		</div>
	</div>
</div>
<div class="container_home">
	<div class="mascara_home"></div>
</div>
<div class="front">
	<div class="container">
		<div class="box_home">
			<div class="row">
				 <div class="col-lg-6 col-md-9 col-xs-6">
					<div class="description">
						<p> 
							No Work on demand você encontra os melhores trabalhos freela que você pode imaginar. Faça seu cadastro
							e corra na frente para descolar aquele Job bem legal. A Work on demand foi criada no intuito de ajudar
							as pessoas a publicar seu perfil e para que as pessoas anuncie vaga de Jobs como: Eventos, Figuração, Modelos,
							Recepção de Festas, dentre outros milhares.
						</p>
					</div>
					
				</div>
				<div class="col-lg-6 col-md-6 col-xs-6">
					<div class="col-lg-12">
						<span class="span_title"> Abra uma conta </span>
					</div>
					<div class="form-group">
						<div class="col-lg-6">
							<input type="text" class="form-control input-lg" placeholder="Nome">
						</div>
						<div class="col-lg-6">
							<input type="text" class="form-control input-lg" placeholder="Sobrenome">
						</div>
						<div class="col-lg-12">
							<input type="email" class="form-control input-lg" placeholder="Email">
						</div>
						<div class="col-lg-12">
							<input type="email" class="form-control input-lg" placeholder="Confirme Email">
						</div>
						<div class="col-lg-12">
							<input type="password" class="form-control input-lg" placeholder="Senha">
						</div>
						<div class="col-lg-12">
							<label> Data de Nascimento </label>
						</div>
						<div class="col-lg-12">
	                            <div data-date-viewmode="years" data-date-format="dd-mm-yyyy" data-date="12-02-2012" id="dpYears" class="input-group date">
	                                <span class="input-group-addon add-on entypo-calendar icon-calendar "></span>
	                                <input type="text" value="12-02-2012" class="form-control" id="ssn2">
	                            </div>
						</div>
						<div class="col-lg-12">
							<div class="radio">
	                            <label>
	                                <input type="radio" name="sexo" id="feminino" value="feminino" checked="">Feminino
	                            </label>
	                            <label>
	                                <input type="radio" name="sexo" id="masculino" value="masculino" checked="">Masculino
	                            </label>
	                        </div>
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div class="col-lg-3">
								<div class="btn_comecar">
									<a href="#" class="plan-button">Cadastrar</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>